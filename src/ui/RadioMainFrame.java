package ui;

import handlers.RadioHandler;
import ui.dialogs.DialogSalirListener;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RadioMainFrame extends JFrame {
    private RadioHandler radioHandler;
    private JMenuBar radioMenuBar;

    public RadioMainFrame(String title, RadioHandler radioHandler) {
        super(title);
        this.radioHandler = radioHandler;
        initUI();
    }

    private void initUI() {
        setSize(600, 400);
        setLocationRelativeTo(null);
        initRadioMenuBar();
        setVisibleRadioMenuBar(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void initRadioMenuBar() {
        radioMenuBar = new JMenuBar();

        JMenu archivoMenu = new JMenu("Archivo");
        JMenuItem resumenItem = new JMenuItem("Inicio");
        resumenItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        radioHandler.showHomePanel();
                    }
                }
        );
        JMenuItem userInfoItem = new JMenuItem("Informaci\u00F3n del usuario");
        userInfoItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        radioHandler.showUserInfoPopUp();
                    }
                }
        );
        JMenuItem hardResetItem = new JMenuItem("Hard reset...");
        hardResetItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        radioHandler.hardReset();
                    }
                }
        );
        JMenuItem logoutItem = new JMenuItem("Cerrar sesi\u00F3n");
        logoutItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        radioHandler.logout();
                    }
                }
        );
        JMenuItem salirItem = new JMenuItem("Salir");
        salirItem.addActionListener(DialogSalirListener.getInstance());

        archivoMenu.add(resumenItem);
        archivoMenu.add(userInfoItem);
        archivoMenu.add(hardResetItem);
        archivoMenu.add(logoutItem);
        archivoMenu.add(salirItem);
        radioMenuBar.add(archivoMenu);

        JMenu programaMenu = new JMenu("Programas");
        JMenuItem altaProgramaMenuItem = new JMenuItem("Nuevo programa");
        altaProgramaMenuItem.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    radioHandler.showNewProgramaPanel();
                }
            }
        );
        JMenuItem mostrarProgramasMenuItem = new JMenuItem("Ver todos");
        mostrarProgramasMenuItem.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    radioHandler.showProgramasTablePanel();
                }
            }
        );

        programaMenu.add(altaProgramaMenuItem);
        programaMenu.add(mostrarProgramasMenuItem);
        radioMenuBar.add(programaMenu);

        JMenu auspicianteMenu = new JMenu("Auspiciantes");
        JMenuItem altaAuspicianteMenuItem = new JMenuItem("Nuevo auspiciante");
        altaAuspicianteMenuItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        radioHandler.showNewAuspiciantePanel();
                    }
                }
        );
        JMenuItem mostrarAuspiciantesMenuItem = new JMenuItem("Ver todos");
        mostrarAuspiciantesMenuItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        radioHandler.showAuspiciantesTablePanel();
                    }
                }
        );

        auspicianteMenu.add(altaAuspicianteMenuItem);
        auspicianteMenu.add(mostrarAuspiciantesMenuItem);
        radioMenuBar.add(auspicianteMenu);

        JMenu productorMenu = new JMenu("Productores");
        JMenuItem altaProductorMenuItem = new JMenuItem("Nuevo productor");
        altaProductorMenuItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        radioHandler.showNewProductorPanel();
                    }
                }
        );
        JMenuItem mostrarProductoresMenuItem = new JMenuItem("Ver todos");
        mostrarProductoresMenuItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        radioHandler.showProductoresTablePanel();
                    }
                }
        );

        productorMenu.add(altaProductorMenuItem);
        productorMenu.add(mostrarProductoresMenuItem);
        radioMenuBar.add(productorMenu);

        JMenu usuarioMenu = new JMenu("Usuarios");
        JMenuItem altaUsuarioMenuItem = new JMenuItem("Nuevo usuario");
        altaUsuarioMenuItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        radioHandler.showNewUsuarioPanel();
                    }
                }
        );
        JMenuItem mostrarUsuariosMenuItem = new JMenuItem("Ver todos");
        mostrarUsuariosMenuItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        radioHandler.showUsuarioTablePanel();
                    }
                }
        );

        usuarioMenu.add(altaUsuarioMenuItem);
        usuarioMenu.add(mostrarUsuariosMenuItem);
        radioMenuBar.add(usuarioMenu);

        setJMenuBar(radioMenuBar);
    }

    public void setVisibleRadioMenuBar(Boolean visible) {
        radioMenuBar.setVisible(visible);
    }

    public void changePanel(JPanel panel) {
        getContentPane().removeAll();
        getContentPane().add(panel);
        getContentPane().validate();
    }
}
