package ui.login;

import entitys.Usuario;
import handlers.RadioHandler;
import ui.components.HorizontalField;
import ui.components.HorizontalTextField;
import ui.components.RadioDefaultPanel;
import ui.dialogs.DialogSalirListener;
import ui.dialogs.RadioDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;

public class LoginPanel extends RadioDefaultPanel {
    private RadioHandler radioHandler;
    private HorizontalTextField userField;
    private JPasswordField passInput;

    public LoginPanel(RadioHandler radioHandler) {
        super(radioHandler);
        this.radioHandler = radioHandler;
        initLoginUI();
    }

    private void initLoginUI() {
        List<Usuario> usuarios = radioHandler.getUsuarios();

        Box loginForm = Box.createVerticalBox();

        userField = new HorizontalTextField("Usuario");
        HorizontalField passField = new HorizontalField("Contrase\u00F1a");
        passInput = new JPasswordField();
        passField.addField(passInput);

        loginForm.add(userField);
        loginForm.add(passField);

        Box footer = Box.createHorizontalBox();
        footer.add(Box.createHorizontalGlue());

        JButton loginBtn = new JButton("Ingresar");
        loginBtn.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        radioHandler.login(userField.getValue(), passInput.getPassword());
                    }
                }
        );

        JButton salirBtn = new JButton("Salir");
        salirBtn.addActionListener(DialogSalirListener.getInstance());

        if (usuarios.isEmpty()) {
            radioHandler.showFirstLoginDialog();

            JButton createAdminBtn = new JButton("Setear/Modificar clave Administrador");
            createAdminBtn.addActionListener(
                    new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            radioHandler.showFirstLoginDialog();
                        }
                    }
            );
            footer.add(createAdminBtn);
            footer.add(Box.createHorizontalStrut(10));
        }

        footer.add(loginBtn);
        footer.add(Box.createHorizontalStrut(10));
        footer.add(salirBtn);
        footer.add(Box.createHorizontalGlue());

        showDefaultRadioPanel("Autenticate para continuar", loginForm, footer);
    }
}
