package ui.dialogs;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DialogSalirListener implements ActionListener {
    private static DialogSalirListener instance;

    public static DialogSalirListener getInstance() {
        if (instance == null)
            instance = new DialogSalirListener();
        return instance;
    }

    public void actionPerformed(ActionEvent e) {
        Object[] options = {"Salir", "Cancelar"};
        int option = JOptionPane.showOptionDialog(null, "\u00BFEst\u00E1s seguro que quer\u00E9s salir?", "Chau chau", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null);
        if (option == JOptionPane.YES_OPTION)
            System.exit(0);
    }
}
