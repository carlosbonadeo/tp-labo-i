package ui.dialogs;

import javax.swing.*;
import java.awt.*;

public abstract class RadioDialog {

    public static void success(String msg) {
        showDialog(new JLabel(msg), null, JOptionPane.INFORMATION_MESSAGE);
    }

    public static void warning(String msg) {
        showDialog(new JLabel(msg), "Algo no est\u00E1 bien", JOptionPane.WARNING_MESSAGE);
    }

    public static void error(String msg) {
        showDialog(new JLabel(msg), "Error", JOptionPane.ERROR_MESSAGE);
    }

    public static void popup(String title, Component content) {
        showDialog(content, title, JOptionPane.PLAIN_MESSAGE);
    }

    private static void showDialog(Component content, String title, int type) {
        JOptionPane.showMessageDialog(null, content, title, type);
    }
}
