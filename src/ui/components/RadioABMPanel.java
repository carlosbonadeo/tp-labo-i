package ui.components;

import handlers.RadioHandler;
import ui.dialogs.DialogConfirm;
import ui.dialogs.RadioDialog;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public abstract class RadioABMPanel extends RadioDefaultPanel {
    Box footer;
    JTable table;

    public RadioABMPanel(RadioHandler radioHandler) {
        super(radioHandler);
        initFooter();
    }

    public void showRadioABMPanel(String label, AbstractTableModel tableModel) {
        table = new JTable(tableModel);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scroll = new JScrollPane(table);

        showDefaultRadioPanel(label, scroll, footer);
    }

    private void initFooter() {
        footer = Box.createHorizontalBox();

        JButton newBtn = new JButton("Nuevo");
        newBtn.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        doNewBtn();
                    }
                }
        );
        JButton editBtn = new JButton("Editar");
        editBtn.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        if (isRowSelected())
                            doEditBtn(table.getSelectedRow());
                    }
                }
        );
        JButton deleteBtn = new JButton("Borrar");
        deleteBtn.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        if (isRowSelected() && DialogConfirm.show("Borrar registro", "\u00BFEst\u00E1s seguro que quer\u00E9s\nborrar el registro?"))
                            doDeleteBtn(table.getSelectedRow());
                    }
                }
        );

        footer.add(newBtn);
        footer.add(Box.createHorizontalStrut(10));
        footer.add(editBtn);
        footer.add(Box.createHorizontalStrut(10));
        footer.add(deleteBtn);
        footer.add(Box.createHorizontalGlue());
    }

    public void addToFooter(Component component) {
        footer.add(component);
    }

    public boolean isRowSelected() {
        if (table.getSelectedRow() == -1) {
            RadioDialog.warning("\u00A1No hay ningún registro seleccionado!");
            return false;
        }
        return true;
    }

    public int getSelectedRow() {
        return table.getSelectedRow();
    }

    protected abstract void doNewBtn();

    protected abstract void doEditBtn(int row);

    protected abstract void doDeleteBtn(int row);
}
