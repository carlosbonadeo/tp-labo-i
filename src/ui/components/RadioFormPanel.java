package ui.components;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;

import handlers.RadioHandler;

public abstract class RadioFormPanel extends RadioDefaultPanel {

	public RadioFormPanel(RadioHandler radioHandler) {
		super(radioHandler);
	}
	
	public void showRadioFormPanel(String label, Component form) {
	    Box footer = Box.createHorizontalBox();
	    footer.add(Box.createHorizontalGlue());

	    JButton actionBtn = new JButton("Guardar");
	    actionBtn.addActionListener(
	            new ActionListener() {
	                public void actionPerformed(ActionEvent e) {
	                	doActionBtn(form);
	                }
	            }
	    );
	    JButton cancelBtn = new JButton("Cancelar");
	    cancelBtn.addActionListener(
	            new ActionListener() {
	                public void actionPerformed(ActionEvent e) {
	                    doCancelBtn();
	                }
	            }
	    );

	    footer.add(actionBtn);
	    footer.add(Box.createHorizontalStrut(10));
	    footer.add(cancelBtn);
	    
	    showDefaultRadioPanel(label, form, footer);
	}
	
	protected abstract void doActionBtn(Component form);
	
	protected abstract void doCancelBtn();
}
