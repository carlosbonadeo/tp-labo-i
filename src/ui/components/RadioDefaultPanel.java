package ui.components;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import handlers.RadioHandler;

import java.awt.*;

public abstract class RadioDefaultPanel extends JPanel {
	
	protected RadioHandler radioHandler;
	
	public RadioDefaultPanel(RadioHandler radioHandler) {
		this.radioHandler = radioHandler;
	}
	
    public void showDefaultRadioPanel(String label, Component form, Component footer) {
        setLayout(new BorderLayout());
        setBorder(new EmptyBorder(20,20,20,20));
        add(new JLabel(label), BorderLayout.NORTH);
        add(form, BorderLayout.CENTER);
        add(footer, BorderLayout.SOUTH);
    }
}
