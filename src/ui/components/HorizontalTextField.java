package ui.components;

import javax.swing.*;
import java.awt.*;

public class HorizontalTextField extends Box {
    private JLabel fieldLabel = new JLabel();
    private JTextField fieldInput = new JTextField(30);

    public HorizontalTextField(String label) {
        super(BoxLayout.X_AXIS);
        initUI(label);
    }

    private void initUI(String label) {
        fieldLabel.setText(label);
        fieldLabel.setPreferredSize(new Dimension(150, 50));

        add(fieldLabel);
        add(Box.createHorizontalStrut(10));
        add(fieldInput);
    }

    public String getValue() {
        return fieldInput.getText();
    }

    public void setValue(String content) {
        fieldInput.setText(content);
    }

    public void setEditable(Boolean editable) {
        fieldInput.setEnabled(editable);
    }
}
