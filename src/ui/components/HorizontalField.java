package ui.components;

import javax.swing.*;
import java.awt.*;

public class HorizontalField extends Box {
    private JLabel fieldLabel = new JLabel();

    public HorizontalField(String label) {
        super(BoxLayout.X_AXIS);
        fieldLabel.setText(label);
        fieldLabel.setPreferredSize(new Dimension(150, 50));
    }

    public void addField(Component field) {
        add(fieldLabel);
        add(Box.createHorizontalStrut(10));
        add(field);
    }
}
