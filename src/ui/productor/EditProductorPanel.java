package ui.productor;

import entitys.Productor;
import handlers.RadioHandler;
import ui.components.RadioFormPanel;

import java.awt.*;

public class EditProductorPanel extends RadioFormPanel {
    public EditProductorPanel(Productor productor, RadioHandler radioHandler) {
        super(radioHandler);
        showRadioFormPanel("Editar productor", new ProductorFields(radioHandler, productor));
    }

    protected void doActionBtn(Component form) {
        ProductorFields productorFields = (ProductorFields) form;
        if (productorFields.validFields())
            radioHandler.editProductor(productorFields.getFields());
    }

    protected void doCancelBtn() {
        radioHandler.showProductoresTablePanel();
    }
}
