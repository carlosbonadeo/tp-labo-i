package ui.productor;

import entitys.Productor;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class ProductorTableModel extends AbstractTableModel {
    private List<Productor> productores;
    private final static int NOMBRE = 0;
    private final static int APELLIDO = 1;
    private final static int PROGRAMA = 2;
    private String[] headers = {"Nombre", "Apellido", "Programa"};

    public ProductorTableModel(List<Productor> productores) {
        this.productores = productores;
    }

    public int getRowCount() {
        return productores.size();
    }

    public int getColumnCount() {
        return headers.length;
    }

    public String getColumnName(int column) {
        return headers[column];
    }

    public Object getValueAt(int row, int column) {
        Productor productor = productores.get(row);

        switch(column) {
            case NOMBRE:
                return productor.getNombre();
            case APELLIDO:
                return productor.getApellido();
            case PROGRAMA:
                return productor.getPrograma();
        }

        // Si no encuentra la columna podr\u00EDa tirar una excepci\u00F3n
        return null;
    }
}
