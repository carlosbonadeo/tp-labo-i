package ui.productor;

import entitys.Productor;
import entitys.Programa;
import handlers.RadioHandler;
import ui.components.HorizontalField;
import ui.components.HorizontalTextField;
import ui.dialogs.RadioDialog;
import ui.programa.ProgramaTableModel;

import javax.swing.*;
import java.util.List;

public class ProductorFields extends Box {
    private RadioHandler radioHandler;
    private Productor productor;
    private List<Programa> programas;
    private HorizontalTextField nombreField;
    private HorizontalTextField apellidoField;
    private HorizontalField programaField;
    private JTable programasTable;

    public ProductorFields(RadioHandler radioHandler) {
        super(BoxLayout.Y_AXIS);
        this.radioHandler = radioHandler;
        this.productor = new Productor();
        initUI();
    }

    public ProductorFields(RadioHandler radioHandler, Productor productor) {
        super(BoxLayout.Y_AXIS);
        this.radioHandler = radioHandler;
        this.productor = productor;
        initUI();
    }

    private void initUI() {
        programas = radioHandler.getProgramas();

        nombreField = new HorizontalTextField("Nombre");
        apellidoField = new HorizontalTextField("Apellido");

        programasTable = new JTable(new ProgramaTableModel(programas));
        programasTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scroll = new JScrollPane(programasTable);

        programaField = new HorizontalField("Programa");
        programaField.addField(scroll);

        nombreField.setValue(productor.getNombre());
        apellidoField.setValue(productor.getApellido());

        add(nombreField);
        add(apellidoField);

        if (productor.getPrograma() != null) {
            for (int i = 0; i < programas.size(); i++) {
                if (productor.getPrograma().equals(programasTable.getValueAt(i, 0))) {
                    programasTable.setRowSelectionInterval(i, i);
                    break;
                }
            }

            HorizontalField programaActualField = new HorizontalField("Actualmente produce el programa");
            programaActualField.addField(new JLabel(productor.getPrograma()));
            add(programaActualField);
        }

        add(programaField);
    }

    public Productor getFields() {
        Programa programa;
        String idPrograma = "";

        if (programasTable.getSelectedRow() > -1) {
            programa = programas.get(programasTable.getSelectedRow());
            idPrograma = String.format("%d-%s", programa.getId(), programa.getNombre());
        }

        productor.setNombre(nombreField.getValue());
        productor.setApellido(apellidoField.getValue());
        productor.setPrograma(idPrograma);

        return productor;
    }

    public boolean validFields() {
        if (nombreField.getValue().isEmpty() || apellidoField.getValue().isEmpty()) {
            RadioDialog.warning("\u00A1El campo Nombre y Apellido son obligatorios!");
            return false;
        }
        return true;
    }
}
