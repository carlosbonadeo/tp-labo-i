package ui.productor;

import handlers.RadioHandler;
import ui.components.RadioFormPanel;

import java.awt.*;

public class NewProductorPanel extends RadioFormPanel {
    public NewProductorPanel(RadioHandler radioHandler) {
        super(radioHandler);
        showRadioFormPanel("Nuevo productor", new ProductorFields(radioHandler));
    }

    protected void doActionBtn(Component form) {
        ProductorFields productorFields = (ProductorFields) form;
        if (productorFields.validFields())
            radioHandler.newProductor(productorFields.getFields());
    }

    protected void doCancelBtn() {
        radioHandler.showProductoresTablePanel();
    }
}
