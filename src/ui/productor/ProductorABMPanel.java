package ui.productor;

import entitys.Productor;
import handlers.RadioHandler;
import ui.components.RadioABMPanel;

import java.util.List;

public class ProductorABMPanel extends RadioABMPanel {
    private List<Productor> productores;

    public ProductorABMPanel(RadioHandler radioHandler) {
        super(radioHandler);
        productores = radioHandler.getProductores();
        showRadioABMPanel("Productores", new ProductorTableModel(productores));
    }

    protected void doNewBtn() {
        radioHandler.showNewProductorPanel();
    }

    protected void doEditBtn(int row) {
        radioHandler.showUpdateProductor(productores.get(row));
    }

    protected void doDeleteBtn(int row) {
        radioHandler.deleteProductor(productores.get(row));
    }
}
