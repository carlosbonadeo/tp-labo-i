package ui.auspiciante;

import entitys.Auspiciante;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class AuspicianteTableModel extends AbstractTableModel {
    private List<Auspiciante> auspiciantes;
    private final static int NOMBRE = 0;
    private final static int CUOTA = 1;
    private final static int ACTIVO = 2;
    private final static int PROGRAMA = 3;
    private String[] headers = {"Nombre", "Cuota mensual", "Activo", "Programa"};

    public AuspicianteTableModel(List<Auspiciante> auspiciantes) {
        this.auspiciantes = auspiciantes;
    }

    public int getRowCount() {
        return auspiciantes.size();
    }

    public int getColumnCount() {
        return headers.length;
    }

    public String getColumnName(int column) {
        return headers[column];
    }

    public Object getValueAt(int row, int column) {
        Auspiciante auspiciante = auspiciantes.get(row);

        switch(column) {
            case NOMBRE:
                return auspiciante.getNombre();
            case CUOTA:
                return auspiciante.getCuota();
            case ACTIVO:
                return auspiciante.isActivo() ? "S\u00ED" : "No";
            case PROGRAMA:
                return auspiciante.getPrograma();
        }

        // Si no encuentra la columna podr\u00EDa tirar una excepci\u00F3n
        return null;
    }
}

