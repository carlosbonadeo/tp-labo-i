package ui.auspiciante;

import entitys.Auspiciante;
import entitys.Programa;
import handlers.RadioHandler;
import ui.components.HorizontalField;
import ui.components.HorizontalTextField;
import ui.dialogs.RadioDialog;
import ui.programa.ProgramaTableModel;

import javax.swing.*;
import java.util.List;

public class AuspicianteFields extends Box {
    private RadioHandler radioHandler;
    private Auspiciante auspiciante;
    private HorizontalTextField nombreField;
    private HorizontalTextField cuotaField;
    private HorizontalField activoField;
    private JCheckBox activoCheck;
    private List<Programa> programas;
    private JTable programasTable;
    private HorizontalField programaField;

    public AuspicianteFields(RadioHandler radioHandler) {
        super(BoxLayout.Y_AXIS);
        this.radioHandler = radioHandler;
        this.auspiciante = new Auspiciante();
        initUI();
    }

    public AuspicianteFields(RadioHandler radioHandler, Auspiciante auspiciante) {
        super(BoxLayout.Y_AXIS);
        this.radioHandler = radioHandler;
        this.auspiciante = auspiciante;
        initUI();
    }

    private void initUI() {
        programas = radioHandler.getProgramas();

        nombreField = new HorizontalTextField("Nombre");
        cuotaField = new HorizontalTextField("Cuota mensual");

        activoField = new HorizontalField("Activo");
        activoCheck = new JCheckBox();
        activoField.addField(activoCheck);

        programasTable = new JTable(new ProgramaTableModel(programas));
        programasTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scroll = new JScrollPane(programasTable);

        programaField = new HorizontalField("Programa");
        programaField.addField(scroll);

        nombreField.setValue(auspiciante.getNombre());
        cuotaField.setValue(String.valueOf(auspiciante.getCuota()));
        activoCheck.setSelected(auspiciante.isActivo());

        add(nombreField);
        add(cuotaField);
        add(activoField);

        if (auspiciante.getPrograma() != null) {
            for (int i = 0; i < programas.size(); i++) {
                if (auspiciante.getPrograma().equals(programasTable.getValueAt(i, 0))) {
                    programasTable.setRowSelectionInterval(i, i);
                    break;
                }
            }

            HorizontalField programaActualField = new HorizontalField("Actualmente es auspiciante del programa");
            programaActualField.addField(new JLabel(auspiciante.getPrograma()));
            add(programaActualField);
        }

        add(programaField);
    }

    public Auspiciante getFields() {
        Programa programa;
        String idPrograma = "";

        if (programasTable.getSelectedRow() > -1) {
            programa = programas.get(programasTable.getSelectedRow());
            idPrograma = String.format("%d-%s", programa.getId(), programa.getNombre());
        }

        auspiciante.setNombre(nombreField.getValue());
        auspiciante.setCuota(Double.parseDouble(cuotaField.getValue()));
        auspiciante.setActivo(activoCheck.isSelected());
        auspiciante.setPrograma(idPrograma);

        return auspiciante;
    }

    public boolean validFields() {
        if (nombreField.getValue().isEmpty() || cuotaField.getValue().isEmpty()) {
            RadioDialog.warning("\u00A1El campo Nombre y Cuota son obligatorios!");
            return false;
        }
        return true;
    }
}
