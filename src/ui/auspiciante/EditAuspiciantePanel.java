package ui.auspiciante;

import entitys.Auspiciante;
import handlers.RadioHandler;
import ui.components.RadioFormPanel;

import java.awt.*;

public class EditAuspiciantePanel extends RadioFormPanel {

	public EditAuspiciantePanel(Auspiciante auspiciante, RadioHandler radioHandler) {
        super(radioHandler);
        showRadioFormPanel("Editar auspiciante", new AuspicianteFields(radioHandler, auspiciante));
    }

    protected void doActionBtn(Component form) {
        AuspicianteFields auspicianteFields = (AuspicianteFields) form;
        if (auspicianteFields.validFields())
            radioHandler.editAuspiciante(auspicianteFields.getFields());
    }

    protected void doCancelBtn() {
        radioHandler.showAuspiciantesTablePanel();
    }
}
