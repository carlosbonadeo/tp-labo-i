package ui.auspiciante;

import entitys.Auspiciante;
import handlers.RadioHandler;
import ui.components.RadioABMPanel;

import java.util.List;

public class AuspicianteABMPanel extends RadioABMPanel {
    private List<Auspiciante> auspiciantes;

    public AuspicianteABMPanel(RadioHandler radioHandler) {
        super(radioHandler);
        auspiciantes = radioHandler.getAuspiciantes();
        showRadioABMPanel("Auspiciantes", new AuspicianteTableModel(auspiciantes));
    }

    protected void doNewBtn() {
        radioHandler.showNewAuspiciantePanel();
    }

    protected void doEditBtn(int row) {
        radioHandler.showUpdateAuspiciante(auspiciantes.get(row));
    }

    protected void doDeleteBtn(int row) {
        radioHandler.deleteAuspiciante(auspiciantes.get(row));
    }
}
