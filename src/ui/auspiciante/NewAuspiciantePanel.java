package ui.auspiciante;

import handlers.RadioHandler;
import ui.components.RadioFormPanel;

import java.awt.*;

public class NewAuspiciantePanel extends RadioFormPanel {

    public NewAuspiciantePanel(RadioHandler radioHandler) {
        super(radioHandler);
        showRadioFormPanel("Nuevo auspiciante", new AuspicianteFields(radioHandler));
    }

    protected void doActionBtn(Component form) {
        AuspicianteFields auspicianteFields = (AuspicianteFields) form;
        if (auspicianteFields.validFields())
            radioHandler.newAuspiciante(auspicianteFields.getFields());
    }

    protected void doCancelBtn() {
        radioHandler.showAuspiciantesTablePanel();
    }
}
