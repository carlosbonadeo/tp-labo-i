package ui.programa;

import entitys.Auspiciante;
import entitys.Programa;
import handlers.RadioHandler;
import ui.auspiciante.AuspicianteTableModel;
import ui.components.RadioDefaultPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class CalcularBalancePanel extends RadioDefaultPanel {

    public CalcularBalancePanel(Programa programa, RadioHandler radioHandler) {
        super(radioHandler);
        initUI(programa);
    }

    private void initUI(Programa programa) {
        String title = String.format("Balance mensual del programa '%d-%s'", programa.getId(), programa.getNombre());
        List<Auspiciante> auspiciantes = radioHandler.getAuspiciantesByPrograma(programa);

        double ingresos = 0;
        int activos = 0;

        for (int i = 0; i < auspiciantes.size(); i++)
            if (auspiciantes.get(i).isActivo()) {
                ingresos += auspiciantes.get(i).getCuota();
                activos++;
            }

        Box layout = Box.createVerticalBox();
        layout.add(Box.createVerticalStrut(10));
        layout.add(new JLabel(String.format("Egresos: $%.2f", programa.getGastos())));
        layout.add(new JLabel(String.format("Ingresos totales: $%.2f", ingresos)));
        layout.add(Box.createVerticalStrut(10));
        layout.add(new JLabel(String.format("Lista de auspiciantes (total %d, %d activos y %d inactivos)", auspiciantes.size(), activos, auspiciantes.size()-activos)));

        JTable table = new JTable(new AuspicianteTableModel(auspiciantes));
        JScrollPane scroll = new JScrollPane(table);
        layout.add(scroll);



        Box footer = Box.createHorizontalBox();
        JButton backBtn = new JButton("Volver");
        backBtn.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        radioHandler.showProgramasTablePanel();
                    }
                }
        );
        footer.add(backBtn);

        showDefaultRadioPanel(title, layout, footer);
    }
}
