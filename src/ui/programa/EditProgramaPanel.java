package ui.programa;

import entitys.Programa;
import handlers.RadioHandler;
import ui.components.RadioFormPanel;

import java.awt.*;

public class EditProgramaPanel extends RadioFormPanel {
    public EditProgramaPanel(Programa programa, RadioHandler radioHandler) {
        super(radioHandler);
        showRadioFormPanel("Editar programa", new ProgramaFields(programa));
    }
    
    protected void doActionBtn(Component form) {
        ProgramaFields programaFields = (ProgramaFields) form;
        if (programaFields.validFields())
            radioHandler.editPrograma(programaFields.getFields());
    }
    
    protected void doCancelBtn() {
    	radioHandler.showProgramasTablePanel();
    }
}
