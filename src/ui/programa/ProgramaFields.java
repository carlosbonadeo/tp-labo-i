package ui.programa;

import entitys.Programa;
import ui.components.HorizontalTextField;
import ui.dialogs.RadioDialog;

import javax.swing.*;

public class ProgramaFields extends Box {
    private Programa programa;
    private HorizontalTextField nombreField;
    private HorizontalTextField horarioField;
    private HorizontalTextField conductoresField;
    private HorizontalTextField gastosField;

    public ProgramaFields() {
        super(BoxLayout.Y_AXIS);
        this.programa = new Programa();
        initUI();
    }

    public ProgramaFields(Programa programa) {
        super(BoxLayout.Y_AXIS);
        this.programa = programa;
        initUI();
    }

    private void initUI() {
        nombreField = new HorizontalTextField("Nombre");
        horarioField = new HorizontalTextField("Horario");
        conductoresField = new HorizontalTextField("Conductores");
        gastosField = new HorizontalTextField("Gastos");

        nombreField.setValue(programa.getNombre());
        horarioField.setValue(programa.getHorario());
        conductoresField.setValue(programa.getConductores());
        gastosField.setValue(String.valueOf(programa.getGastos()));

        add(nombreField);
        add(horarioField);
        add(conductoresField);
        add(gastosField);
    }

    public Programa getFields() {
        programa.setNombre(nombreField.getValue());
        programa.setHorario(horarioField.getValue());
        programa.setConductores(conductoresField.getValue());
        programa.setGastos(Double.parseDouble(gastosField.getValue()));

        return programa;
    }

    public boolean validFields() {
        if (nombreField.getValue().isEmpty() || horarioField.getValue().isEmpty() || conductoresField.getValue().isEmpty() || gastosField.getValue().isEmpty()) {
            RadioDialog.warning("\u00A1Todos los campos son obligatorios!");
            return false;
        }
        return true;
    }
}
