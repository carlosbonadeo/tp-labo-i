package ui.programa;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;

import entitys.Programa;
import handlers.RadioHandler;
import ui.components.RadioABMPanel;

public class ProgramaABMPanel extends RadioABMPanel {
    List<Programa> programas;

    public ProgramaABMPanel(RadioHandler radioHandler) {
    	super(radioHandler);
        programas = radioHandler.getProgramas();
        initUI();
    }

    private void initUI() {
        JButton verProductoresBtn = new JButton("Productores");
        verProductoresBtn.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        if (isRowSelected())
                            radioHandler.showProductoresDialog(programas.get(getSelectedRow()));
                    }
                }
        );

        JButton calcularBalanceBtn = new JButton("Balance");
        calcularBalanceBtn.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        if (isRowSelected())
                            radioHandler.showCalcularBalancePanel(programas.get(getSelectedRow()));
                    }
                }
        );

        addToFooter(verProductoresBtn);
        addToFooter(Box.createHorizontalStrut(10));
        addToFooter(calcularBalanceBtn);

        showRadioABMPanel("Programas", new ProgramaTableModel(programas));
    }

    protected void doNewBtn() {
        radioHandler.showNewProgramaPanel();
    }

    protected void doEditBtn(int row) {
        radioHandler.showUpdatePrograma(programas.get(row));
    }

    protected void doDeleteBtn(int row) {
        radioHandler.deletePrograma(programas.get(row));
    }
}
