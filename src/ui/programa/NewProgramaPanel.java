package ui.programa;

import handlers.RadioHandler;
import ui.components.RadioFormPanel;

import java.awt.*;

public class NewProgramaPanel extends RadioFormPanel {

    public NewProgramaPanel(RadioHandler radioHandler) {
    	super(radioHandler);
    	showRadioFormPanel("Nuevo programa", new ProgramaFields());
    }

    protected void doActionBtn(Component form) {
        ProgramaFields programaFields = (ProgramaFields) form;
        if (programaFields.validFields())
    	    radioHandler.newPrograma(programaFields.getFields());
    }
    
    protected void doCancelBtn() {
    	radioHandler.showHomePanel();
    }
}
