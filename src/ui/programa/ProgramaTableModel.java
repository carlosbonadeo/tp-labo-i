package ui.programa;

import entitys.Programa;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class ProgramaTableModel extends AbstractTableModel {
    private List<Programa> programas;
    private final static int NOMBRE = 0;
    private final static int HORARIO = 1;
    private final static int CONDUCTORES = 2;
    private final static int GASTOS = 3;
    private String[] headers = {"Nombre", "Horario", "Conductores", "Gastos"};

    public ProgramaTableModel(List<Programa> programas) {
        this.programas = programas;
    }

    public int getRowCount() {
        return programas.size();
    }

    public int getColumnCount() {
        return headers.length;
    }

    public String getColumnName(int column) {
        return headers[column];
    }

    public Object getValueAt(int row, int column) {
        Programa programa = programas.get(row);

        switch(column) {
            case NOMBRE:
                return programa.getId() + "-" + programa.getNombre();
            case HORARIO:
                return programa.getHorario();
            case CONDUCTORES:
                return programa.getConductores();
            case GASTOS:
                return programa.getGastos();
        }

        // Si no encuentra la columna podr\u00EDa tirar una excepci\u00F3n
        return null;
    }
}

