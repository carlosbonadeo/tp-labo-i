package ui.usuario;

import entitys.Usuario;
import handlers.RadioHandler;
import ui.components.HorizontalField;
import ui.components.HorizontalTextField;
import ui.dialogs.RadioDialog;

import javax.swing.*;
import java.util.Arrays;
import java.util.List;

public class UsuarioFields extends Box {
    private RadioHandler radioHandler;
    private Usuario usuario;
    private boolean newUser;
    private HorizontalTextField usernameField;
    private HorizontalTextField nameField;
    private HorizontalTextField mailField;
    private HorizontalField rolField;
    private JComboBox rolCombo;
    private HorizontalField passwordField;
    private JPasswordField passwordInput;
    private HorizontalField passwordConfirmField;
    private JPasswordField passwordConfirmInput;

    public UsuarioFields(RadioHandler radioHandler) {
        super(BoxLayout.Y_AXIS);
        this.radioHandler = radioHandler;
        this.usuario = new Usuario();
        newUser = true;
        initUI();
    }

    public UsuarioFields(RadioHandler radioHandler, Usuario usuario) {
        super(BoxLayout.Y_AXIS);
        this.radioHandler = radioHandler;
        this.usuario = usuario;
        newUser = false;
        initUI();
    }

    private void initUI() {
        usernameField = new HorizontalTextField("Nombre de usuario");
        nameField = new HorizontalTextField("Nombre");
        mailField = new HorizontalTextField("Mail");

        rolField = new HorizontalField("Rol");
        rolCombo = new JComboBox();

        List<String> roles = radioHandler.getRoles();
        for (int i = 0; i < roles.size(); i++)
            rolCombo.addItem(roles.get(i));
        rolField.addField(rolCombo);

        passwordField = new HorizontalField("Contrase\u00F1a");
        passwordInput = new JPasswordField();
        passwordField.addField(passwordInput);

        passwordConfirmField = new HorizontalField("Confirmar contrase\u00F1a");
        passwordConfirmInput = new JPasswordField();
        passwordConfirmField.addField(passwordConfirmInput);

        usernameField.setValue(usuario.getUsername());
        if (!newUser)
            usernameField.setEditable(false);

        nameField.setValue(usuario.getName());
        mailField.setValue(usuario.getMail());
        rolCombo.setSelectedIndex(usuario.getRol());
        if (!newUser && usuario.getUsername().equals("ADMIN"))
            rolCombo.setEnabled(false);

        add(usernameField);
        add(nameField);
        add(mailField);
        add(rolField);
        add(passwordField);
        add(passwordConfirmField);
    }

    public Usuario getFields() {
        usuario.setUsername(usernameField.getValue());
        usuario.setName(nameField.getValue());
        usuario.setMail(mailField.getValue());
        usuario.setRol(rolCombo.getSelectedIndex());

        if (!keepPassword())
            usuario.setPassword(new String(passwordInput.getPassword()));

        return usuario;
    }

    public boolean validFields() {
        if (usernameField.getValue().isEmpty()) {
            RadioDialog.warning("\u00A1El campo 'Nombre de usuario' es obligatorio!");
            return false;
        }

//        if (newUser && radioHandler.getUsuarioByUsername(usernameField.getValue()) != null) {
//            RadioDialog.warning("\u00A1El usuario ya existe!");
//            return false;
//        }


        if (keepPassword()) {
            return true;
        }

        if (passwordInput.getPassword().length == 0 && passwordConfirmInput.getPassword().length == 0) {
            RadioDialog.warning("\u00A1Contrase\u00F1a inv\u00E1lida!");
            return false;
        }

        if (!Arrays.equals(passwordInput.getPassword(), passwordConfirmInput.getPassword())) {
            RadioDialog.warning("\u00A1Las contrase\u00F1as no coinciden!");
            return false;
        }

        return true;
    }

    private boolean keepPassword() {
        return !newUser && passwordInput.getPassword().length == 0 && passwordConfirmInput.getPassword().length == 0;
    }
}
