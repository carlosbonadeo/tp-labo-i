package ui.usuario;

import entitys.Usuario;
import handlers.RadioHandler;
import ui.components.RadioABMPanel;

import java.util.List;

public class UsuarioABMPanel extends RadioABMPanel {
    private List<Usuario> usuarios;

    public UsuarioABMPanel(RadioHandler radioHandler) {
        super(radioHandler);
        usuarios = radioHandler.getUsuarios();
        showRadioABMPanel("Usuarios", new UsuarioTableModel(usuarios, radioHandler.getRoles()));
    }

    protected void doNewBtn() {
        radioHandler.showNewUsuarioPanel();
    }

    protected void doEditBtn(int row) {
        radioHandler.showUpdateUsuario(usuarios.get(row));
    }

    protected void doDeleteBtn(int row) {
        radioHandler.deleteUsuario(usuarios.get(row));
    }
}
