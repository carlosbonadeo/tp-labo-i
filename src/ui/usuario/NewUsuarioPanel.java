package ui.usuario;

import handlers.RadioHandler;
import ui.components.RadioFormPanel;

import java.awt.*;

public class NewUsuarioPanel extends RadioFormPanel {
    public NewUsuarioPanel(RadioHandler radioHandler) {
        super(radioHandler);
        showRadioFormPanel("Nuevo usuario", new UsuarioFields(radioHandler));
    }

    protected void doActionBtn(Component form) {
        UsuarioFields usuarioFields = (UsuarioFields) form;
        if (usuarioFields.validFields())
            radioHandler.newUsuario(usuarioFields.getFields());
    }

    protected void doCancelBtn() {
        radioHandler.showUsuarioTablePanel();
    }
}
