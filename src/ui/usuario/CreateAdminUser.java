package ui.usuario;

import entitys.Usuario;
import handlers.RadioHandler;
import ui.components.HorizontalField;
import ui.dialogs.RadioDialog;

import javax.swing.*;
import java.util.Arrays;

public class CreateAdminUser {
    private RadioHandler radioHandler;
    private HorizontalField usernameField;
    private HorizontalField passwordField;
    private JPasswordField passwordInput;
    private HorizontalField passwordConfirmField;
    private JPasswordField passwordConfirmInput;

    public CreateAdminUser(RadioHandler radioHandler) {
        this.radioHandler = radioHandler;
        initUI();
    }

    private void initUI() {
        final String TITLE = "Crear usuario Administrador";
        final Object[] OPTIONS = {"Guardar", "Cancelar"};

        Box content = Box.createVerticalBox();

        usernameField = new HorizontalField("Usuario");
        usernameField.addField(new JLabel("ADMIN"));

        passwordField = new HorizontalField("Contrase\u00F1a");
        passwordInput = new JPasswordField();
        passwordField.addField(passwordInput);

        passwordConfirmField = new HorizontalField("Confirmar contrase\u00F1a");
        passwordConfirmInput = new JPasswordField();
        passwordConfirmField.addField(passwordConfirmInput);

        content.add(usernameField);
        content.add(passwordField);
        content.add(passwordConfirmField);

        int option = JOptionPane.showOptionDialog(null, content, TITLE, JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, OPTIONS, null);

        if (option == JOptionPane.YES_OPTION) {
            if (!Arrays.equals(passwordInput.getPassword(), passwordConfirmInput.getPassword()))
                RadioDialog.warning("\u00A1Las contrase\u00F1as no coinciden!");
            else {
//                Usuario admin = new Usuario();
//                admin.setUsername("ADMIN");
//                admin.setName("Administrador");
//                admin.setRol(0);
//                admin.setPassword(new String(passwordInput.getPassword()));


                radioHandler.setAdminPassword(passwordInput.getPassword());
//                newUsuario(admin);

            }
        }
    }
}
