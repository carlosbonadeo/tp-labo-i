package ui.usuario;

import entitys.Usuario;
import handlers.RadioHandler;
import ui.components.RadioFormPanel;

import java.awt.*;

public class EditUsuarioPanel extends RadioFormPanel {
    public EditUsuarioPanel(Usuario usuario, RadioHandler radioHandler) {
        super(radioHandler);
        showRadioFormPanel("Editar usuario", new UsuarioFields(radioHandler, usuario));
    }

    protected void doActionBtn(Component form) {
        UsuarioFields usuarioFields = (UsuarioFields) form;
        if (usuarioFields.validFields())
            radioHandler.editUsuario(usuarioFields.getFields());
    }

    protected void doCancelBtn() {
        radioHandler.showUsuarioTablePanel();
    }
}
