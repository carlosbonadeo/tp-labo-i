package ui.usuario;

import entitys.Usuario;
import handlers.RadioHandler;
import ui.dialogs.RadioDialog;

import javax.swing.*;

public class UserInfoPanel extends JPanel {
    RadioHandler radioHandler;

    public UserInfoPanel(RadioHandler radioHandler, Usuario currentUser) {
        this.radioHandler = radioHandler;
        initUI(currentUser);
    }

    private void initUI(Usuario user) {
        Box content = Box.createVerticalBox();

        content.add(new JLabel("Usuario: " + user.getUsername()));
        content.add(new JLabel("Nombre: " + user.getName()));
        content.add(new JLabel("Mail: " + user.getMail()));
        content.add(new JLabel("Rol: " + radioHandler.getRol(user.getRol())));

        RadioDialog.popup("Usuario logueado", content);
    }
}
