package ui.usuario;

import entitys.Usuario;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class UsuarioTableModel extends AbstractTableModel {
    private List<Usuario> usuarios;
    private List<String> roles;
    private final static int USERNAME = 0;
    private final static int NOMBRE = 1;
    private final static int MAIL = 2;
    private final static int ROL = 3;
    private String[] headers = {"Usuario", "Nombre", "Mail", "Rol"};

    public UsuarioTableModel(List<Usuario> usuarios, List<String> roles) {
        this.usuarios = usuarios;
        this.roles = roles;
    }

    public int getRowCount() {
        return usuarios.size();
    }

    public int getColumnCount() {
        return headers.length;
    }

    public String getColumnName(int column) {
        return headers[column];
    }

    public Object getValueAt(int row, int column) {
        Usuario usuario = usuarios.get(row);

        switch(column) {
            case USERNAME:
                return usuario.getUsername();
            case NOMBRE:
                return usuario.getName();
            case MAIL:
                return usuario.getMail();
            case ROL:
                return roles.get(usuario.getRol());
        }

        // Si no encuentra la columna podr\u00EDa tirar una excepci\u00F3n
        return null;
    }
}
