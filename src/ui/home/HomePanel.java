package ui.home;

import entitys.Auspiciante;
import entitys.Programa;
import handlers.RadioHandler;
import ui.components.RadioDefaultPanel;

import javax.swing.*;
import java.util.List;

public class HomePanel extends RadioDefaultPanel {

    public HomePanel(RadioHandler radioHandler) {
       super(radioHandler);
        initUI();
    }

    private void initUI() {
        List<Programa> programas = radioHandler.getProgramas();
        List<Auspiciante> auspiciantes = radioHandler.getAuspiciantes();
        double montoProgramas = 0;
        double montoAuspiciantes = 0;

        for(int i = 0; i < programas.size(); i++)
            montoProgramas += programas.get(i).getGastos();

        for(int i = 0; i < auspiciantes.size(); i++)
            montoAuspiciantes += auspiciantes.get(i).getCuota();

        Box dashboard = Box.createVerticalBox();
        dashboard.add(Box.createVerticalStrut(50));
        dashboard.add(new JLabel("USUARIO: " + radioHandler.getCurrentUser().getUsername()));
        dashboard.add(Box.createVerticalStrut(20));
        dashboard.add(new JLabel("RESUMEN"));
        dashboard.add(Box.createVerticalStrut(10));
        dashboard.add(new JLabel("Hay " + programas.size() + " programas, que suman $" + String.format("%.2f", montoProgramas) + " en egresos."));
        dashboard.add(Box.createVerticalStrut(10));
        dashboard.add(new JLabel("Hay " + auspiciantes.size() + " auspiciantes, que suman $" + String.format("%.2f", montoAuspiciantes) + " en ingresos."));
        dashboard.add(Box.createVerticalStrut(10));
        dashboard.add(new JLabel("-------"));
        dashboard.add(Box.createVerticalStrut(10));
        dashboard.add(new JLabel("Al d\u00EDa de la fecha, el balance de la radio es: $" + String.format("%.2f", montoAuspiciantes - montoProgramas)));

        showDefaultRadioPanel("\u00A1Bienvenido al sistema de radio!", dashboard, new JLabel("Universidad de Palermo, 2019"));
    }
}
