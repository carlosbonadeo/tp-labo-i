package entitys;

public class Auspiciante {
    private int id;
    private String nombre;
    private double cuota;
    private boolean activo = true;
    private String programa;

    public Auspiciante() {}

    public Auspiciante(int id, String nombre, double cuota, boolean activo, String programa) {
        this.id = id;
        this.nombre = nombre;
        this.cuota = cuota;
        this.activo = activo;
        this.programa = programa;
    }

    public String toString() {
        return "Auspiciante{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", cuota=" + cuota +
                ", activo=" + activo +
                ", programa=" + programa +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getCuota() {
        return cuota;
    }

    public void setCuota(double cuota) {
        this.cuota = cuota;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }
}
