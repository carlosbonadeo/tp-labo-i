package entitys;

public class Usuario {
    private int id;
    private String username;
    private String name;
    private String mail;
    private int rol;
    private String password;

    public Usuario() {
        rol = 1;
    }

    public Usuario(int id, String username, String name, String mail, int rol, String password) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.mail = mail;
        this.rol = rol;
        this.password = password;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", mail='" + mail + '\'' +
                ", rol=" + rol +
                ", password='" + password + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public int getRol() {
        return rol;
    }

    public void setRol(int rol) {
        this.rol = rol;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
