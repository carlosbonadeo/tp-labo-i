package entitys;

public class Programa {
    private int id;
    private String nombre;
    private String horario;
    private String conductores;
    private double gastos;

    public Programa() {}

    public Programa(int id, String nombre, String horario, String conductores, double gastos) {
        this.id = id;
        this.nombre = nombre;
        this.horario = horario;
        this.conductores = conductores;
        this.gastos = gastos;
    }

    public String toString() {
        return "Programa{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", horario='" + horario + '\'' +
                ", conductores='" + conductores + '\'' +
                ", gastos=" + gastos +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public double getGastos() {
        return gastos;
    }

    public void setGastos(double gastos) {
        this.gastos = gastos;
    }

    public String getConductores() {
        return conductores;
    }

    public void setConductores(String conductores) {
        this.conductores = conductores;
    }

}
