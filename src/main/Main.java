package main;

import db.TableManager;
import exceptions.RadioException;
import handlers.RadioHandler;

public class Main {

    /**
     * Sistema de manejo de una radio:
     * Se trata de un sistema de manejo de programaci\u00F3n de una radio. En las estaciones de radios hay programas con
     * sus respectivos horarios, los conductores y productores de cada programa, etc.
     * Cada programa tambien cuenta con auspiciantes, los cuales aportan dinero por tiempo de aire para su publicidad.
     * A fin de mes, el productor debera poder saber cuales fueron los gastos, como los ingresos,
     * y asi poder calcular un balance.
     */

    public static void main(String[] args) {
    	RadioHandler radioHandler = new RadioHandler();
        TableManager tm = new TableManager();

        try {
        	tm.createUserTable();
        } catch(RadioException e) {
            System.out.println(e.getMessage());
        }try {
        	tm.createProgramaTable();
        } catch(RadioException e) {
            System.out.println(e.getMessage());
        }
        try {
        	tm.createAuspicianteTable();
        } catch(RadioException e) {
        	System.out.println(e.getMessage());
        }
        try {
        	tm.createProductorTable();
        } catch(RadioException e) {
        	System.out.println(e.getMessage());
        }
              
        radioHandler.initMainFrame();
    }
}
