package db;

import entitys.Auspiciante;
import entitys.Programa;
import exceptions.RadioException;
import interfaces.AuspicianteDAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AuspicianteImplDAO implements AuspicianteDAO {
    private static final String NEW_AUSPICIANTE_QUERY = "INSERT INTO auspiciantes (nombre, cuota, activo, programa) VALUES (?, ?, ?, ?)";
    private static final String DELETE_AUSPICIANTE_QUERY = "DELETE FROM auspiciantes WHERE id = ?";
    private static final String EDIT_AUSPICIANTE_QUERY = "UPDATE auspiciantes set nombre = ?, cuota = ?, activo = ?, programa = ? WHERE id = ?";
    private static final String GET_AUSPICIANTES_QUERY = "SELECT * FROM auspiciantes";
    private static final String GET_AUSPICIANTES_BY_PROGRAMA_QUERY = "SELECT * FROM auspiciantes WHERE programa = ?";

    public void newAuspiciante(Auspiciante auspiciante) throws RadioException {
        Connection c = DBManager.getInstance().connect();

        try {
            PreparedStatement ps = c.prepareStatement(NEW_AUSPICIANTE_QUERY);
            ps.setString(1, auspiciante.getNombre());
            ps.setDouble(2, auspiciante.getCuota());
            ps.setBoolean(3, auspiciante.isActivo());
            ps.setString(4, auspiciante.getPrograma());
            ps.executeUpdate();

            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            throw new RadioException("Ocurri\u00F3 un error al agregar el nuevo auspiciante", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    public void deleteAuspiciante(Auspiciante auspiciante) throws RadioException {
        Connection c = DBManager.getInstance().connect();

        try {
            PreparedStatement ps = c.prepareStatement(DELETE_AUSPICIANTE_QUERY);
            ps.setInt(1, auspiciante.getId());
            ps.executeUpdate();
            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                //no hago nada
            }
            throw new RadioException("Ocurri\u00F3 un error al borrar el auspiciante seleccionado", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                //no hago nada
            }
        }
    }

    public void editAuspiciante(Auspiciante auspiciante) throws RadioException {
        Connection c = DBManager.getInstance().connect();

        try {
            PreparedStatement ps = c.prepareStatement(EDIT_AUSPICIANTE_QUERY);
            ps.setString(1, auspiciante.getNombre());
            ps.setDouble(2, auspiciante.getCuota());
            ps.setBoolean(3, auspiciante.isActivo());
            ps.setString(4, auspiciante.getPrograma());
            ps.setInt(5, auspiciante.getId());
            ps.executeUpdate();

            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                //no hago nada
            }
            throw new RadioException("Ocurri\u00F3 un error al actualizar el auspiciante seleccionado", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                //no hago nada
            }
        }
    }

    public List<Auspiciante> getAuspiciantes() throws RadioException {
        Connection c = DBManager.getInstance().connect();
        List<Auspiciante> auspiciantes = new ArrayList<Auspiciante>();

        try {
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(GET_AUSPICIANTES_QUERY);

            while (rs.next())
                auspiciantes.add(
                    new Auspiciante(
                        rs.getInt("id"),
                        rs.getString("nombre"),
                        rs.getDouble("cuota"),
                        rs.getBoolean("activo"),
                        rs.getString("programa")
                    )
                );

            return auspiciantes;
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                //no hago nada
            }
            throw new RadioException("Ocurri\u00F3 un error al obtener todos los auspiciantes", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                //no hago nada
            }
        }
    }

    public List<Auspiciante> getAuspiciantesByPrograma(Programa programa) throws RadioException {
        String programaId = programa.getId() + "-" + programa.getNombre();
        Connection c = DBManager.getInstance().connect();
        List<Auspiciante> auspiciantes = new ArrayList<Auspiciante>();

        try {
            PreparedStatement ps = c.prepareStatement(GET_AUSPICIANTES_BY_PROGRAMA_QUERY);
            ps.setString(1, programaId);
            ResultSet rs = ps.executeQuery();

            while (rs.next())
                auspiciantes.add(
                        new Auspiciante(
                                rs.getInt("id"),
                                rs.getString("nombre"),
                                rs.getDouble("cuota"),
                                rs.getBoolean("activo"),
                                rs.getString("programa")
                        )
                );

            return auspiciantes;
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                //no hago nada
            }
            throw new RadioException("Ocurri\u00F3 un error al obtener los auspiciantes del programa " + programaId, e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                //no hago nada
            }
        }
    }
}
