package db;

import entitys.Programa;
import exceptions.RadioException;
import interfaces.ProgramaDAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProgramaImplDAO implements ProgramaDAO {
    private static final String NEW_PROGRAMA_QUERY = "INSERT INTO programas (nombre, horario, conductores, gastos) VALUES (?, ?, ?, ?)";
    private static final String DELETE_PROGRAMA_QUERY = "DELETE FROM programas WHERE id = ?";
    private static final String EDIT_PROGRAMA_QUERY = "UPDATE programas set nombre = ?, horario = ?, conductores = ?, gastos = ? WHERE id = ?";
    private static final String GET_PROGRAMAS_QUERY = "SELECT * FROM programas";

    public void newPrograma(Programa programa) throws RadioException {
        Connection c = DBManager.getInstance().connect();

        try {
            PreparedStatement ps = c.prepareStatement(NEW_PROGRAMA_QUERY);
            ps.setString(1, programa.getNombre());
            ps.setString(2, programa.getHorario());
            ps.setString(3, programa.getConductores());
            ps.setDouble(4, programa.getGastos());
            ps.executeUpdate();

            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            throw new RadioException("Ocurri\u00F3 un error al agregar el nuevo programa", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    public void deletePrograma(Programa programa) throws RadioException {
        Connection c = DBManager.getInstance().connect();

        try {
            PreparedStatement ps = c.prepareStatement(DELETE_PROGRAMA_QUERY);
            ps.setInt(1, programa.getId());
            ps.executeUpdate();

            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                //no hago nada
            }
            throw new RadioException("Ocurri\u00F3 un error al borrar el programa seleccionado", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                //no hago nada
            }
        }
    }

    public void editPrograma(Programa programa) throws RadioException {
        Connection c = DBManager.getInstance().connect();

        try {
            PreparedStatement ps = c.prepareStatement(EDIT_PROGRAMA_QUERY);
            ps.setString(1, programa.getNombre());
            ps.setString(2, programa.getHorario());
            ps.setString(3, programa.getConductores());
            ps.setDouble(4, programa.getGastos());
            ps.setInt(5, programa.getId());

            ps.executeUpdate();

            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                //no hago nada
            }
            throw new RadioException("Ocurri\u00F3 un error al actualizar el programa seleccionado", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                //no hago nada
            }
        }
    }

    public List<Programa> getProgramas() throws RadioException {
        Connection c = DBManager.getInstance().connect();
        List<Programa> programas = new ArrayList<Programa>();

        try {
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(GET_PROGRAMAS_QUERY);

            while (rs.next())
                programas.add(
                    new Programa(
                        rs.getInt("id"),
                        rs.getString("nombre"),
                        rs.getString("horario"),
                        rs.getString("conductores"),
                        rs.getDouble("gastos")
                    )
                );

            return programas;
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                //no hago nada
            }
            throw new RadioException("Ocurri\u00F3 un error al obtener todos los programas", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                //no hago nada
            }
        }
    }
}
