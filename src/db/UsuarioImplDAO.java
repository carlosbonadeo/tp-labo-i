package db;

import entitys.Usuario;
import exceptions.RadioException;
import interfaces.UsuarioDAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UsuarioImplDAO implements UsuarioDAO {
    private static final String NEW_USUARIO_QUERY = "INSERT INTO usuarios (username, nombre, mail, rol, password) VALUES (?, ?, ?, ?, ?)";
    private static final String DELETE_USUARIO_QUERY = "DELETE FROM usuarios WHERE id = ?";
    private static final String EDIT_USUARIO_QUERY = "UPDATE usuarios set username = ?, nombre = ?, mail = ?, rol = ?, password = ? WHERE id = ?";
    private static final String GET_USUARIOS_QUERY = "SELECT * FROM usuarios";
    private static final String GET_USUARIO_BY_USERNAME_QUERY = "SELECT * FROM usuarios WHERE username = ?";
    private static final String DELETE_USUARIOS_QUERY = "DELETE FROM usuarios";

    public void newUsuario(Usuario usuario) throws RadioException {
        Connection c = DBManager.getInstance().connect();

        try {
            PreparedStatement ps = c.prepareStatement(NEW_USUARIO_QUERY);
            ps.setString(1, usuario.getUsername());
            ps.setString(2, usuario.getName());
            ps.setString(3, usuario.getMail());
            ps.setInt(4, usuario.getRol());
            ps.setString(5, usuario.getPassword());
            ps.executeUpdate();

            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            throw new RadioException("Ocurri\u00F3 un error al agregar el nuevo usuario", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    public void deleteUsuario(Usuario usuario) throws RadioException {
        Connection c = DBManager.getInstance().connect();

        try {
            PreparedStatement ps = c.prepareStatement(DELETE_USUARIO_QUERY);
            ps.setInt(1, usuario.getId());
            ps.executeUpdate();

            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                //no hago nada
            }
            throw new RadioException("Ocurri\u00F3 un error al borrar el usuario seleccionado", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                //no hago nada
            }
        }
    }

    public void editUsuario(Usuario usuario) throws RadioException {
        Connection c = DBManager.getInstance().connect();

        try {
            PreparedStatement ps = c.prepareStatement(EDIT_USUARIO_QUERY);
            ps.setString(1, usuario.getUsername());
            ps.setString(2, usuario.getName());
            ps.setString(3, usuario.getMail());
            ps.setInt(4, usuario.getRol());
            ps.setString(5, usuario.getPassword());
            ps.setInt(6, usuario.getId());

            ps.executeUpdate();

            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                //no hago nada
            }
            throw new RadioException("Ocurri\u00F3 un error al actualizar el usuario seleccionado", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                //no hago nada
            }
        }
    }

    public List<Usuario> getUsuarios() throws RadioException {
        Connection c = DBManager.getInstance().connect();
        List<Usuario> usuarios = new ArrayList<Usuario>();

        try {
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(GET_USUARIOS_QUERY);

            while (rs.next())
                usuarios.add(
                    new Usuario(
                        rs.getInt("id"),
                        rs.getString("username"),
                        rs.getString("nombre"),
                        rs.getString("mail"),
                        rs.getInt("rol"),
                        rs.getString("password")
                    )
                );

            return usuarios;
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                //no hago nada
            }
            throw new RadioException("Ocurri\u00F3 un error al obtener todos los usuarios", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                //no hago nada
            }
        }
    }

    public Usuario getUsuarioByUsername(String username) throws RadioException {
        Connection c = DBManager.getInstance().connect();

        try {
            PreparedStatement ps = c.prepareStatement(GET_USUARIO_BY_USERNAME_QUERY);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();

            if (rs.next())
                return new Usuario(
                    rs.getInt("id"),
                    rs.getString("username"),
                    rs.getString("nombre"),
                    rs.getString("mail"),
                    rs.getInt("rol"),
                    rs.getString("password")
                );

            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                //no hago nada
            }
            throw new RadioException("Ocurri\u00F3 un error al obtener el usuario", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                //no hago nada
            }
        }

        return null;
    }

    public void deleteUsuarios() throws RadioException {
        Connection c = DBManager.getInstance().connect();

        try {
            PreparedStatement ps = c.prepareStatement(DELETE_USUARIOS_QUERY);
            ps.executeUpdate();

            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                //no hago nada
            }
            throw new RadioException("Ocurri\u00F3 un error al borrar todos los usuarios", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                //no hago nada
            }
        }
    }
}
