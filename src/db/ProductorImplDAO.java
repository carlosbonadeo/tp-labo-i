package db;

import entitys.Productor;
import entitys.Programa;
import exceptions.RadioException;
import interfaces.ProductorDAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductorImplDAO implements ProductorDAO {
    private static final String NEW_PRODUCTOR_QUERY = "INSERT INTO productores (nombre, apellido, programa) VALUES (?, ?, ?)";
    private static final String DELETE_PRODUCTOR_QUERY = "DELETE FROM productores WHERE id = ?";
    private static final String EDIT_PRODUCTOR_QUERY = "UPDATE productores set nombre = ?, apellido = ?, programa = ? WHERE id = ?";
    private static final String GET_PRODUCTORES_QUERY = "SELECT * FROM productores";
    private static final String GET_PRODUCTORES_BY_PROGRAMA_QUERY = "SELECT * FROM productores WHERE programa = ?";

    public void newProductor(Productor productor) throws RadioException {
        Connection c = DBManager.getInstance().connect();

        try {
            PreparedStatement ps = c.prepareStatement(NEW_PRODUCTOR_QUERY);
            ps.setString(1, productor.getNombre());
            ps.setString(2, productor.getApellido());
            ps.setString(3, productor.getPrograma());
            ps.executeUpdate();

            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            throw new RadioException("Ocurri\u00F3 un error al agregar el nuevo productor", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    public void deleteProductor(Productor productor) throws RadioException {
        Connection c = DBManager.getInstance().connect();

        try {
            PreparedStatement ps = c.prepareStatement(DELETE_PRODUCTOR_QUERY);
            ps.setInt(1, productor.getId());
            ps.executeUpdate();
            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                //no hago nada
            }
            throw new RadioException("Ocurri\u00F3 un error al borrar el productor seleccionado", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                //no hago nada
            }
        }
    }

    public void editProductor(Productor productor) throws RadioException {
        Connection c = DBManager.getInstance().connect();

        try {
            PreparedStatement ps = c.prepareStatement(EDIT_PRODUCTOR_QUERY);
            ps.setString(1, productor.getNombre());
            ps.setString(2, productor.getApellido());
            ps.setString(3, productor.getPrograma());
            ps.setInt(4, productor.getId());
            ps.executeUpdate();

            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                //no hago nada
            }
            throw new RadioException("Ocurri\u00F3 un error al actualizar el productor seleccionado", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                //no hago nada
            }
        }
    }

    public List<Productor> getProductores() throws RadioException {
        Connection c = DBManager.getInstance().connect();
        List<Productor> productores = new ArrayList<Productor>();

        try {
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(GET_PRODUCTORES_QUERY);

            while (rs.next())
                productores.add(
                        new Productor(
                                rs.getInt("id"),
                                rs.getString("nombre"),
                                rs.getString("apellido"),
                                rs.getString("programa")
                        )
                );

            return productores;
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                //no hago nada
            }
            throw new RadioException("Ocurri\u00F3 un error al obtener todos los productores", e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                //no hago nada
            }
        }
    }

    public List<Productor> getProductoresByPrograma(Programa programa) throws RadioException {
        String programaId = programa.getId() + "-" + programa.getNombre();
        Connection c = DBManager.getInstance().connect();
        List<Productor> auspiciantes = new ArrayList<Productor>();

        try {
            PreparedStatement ps = c.prepareStatement(GET_PRODUCTORES_BY_PROGRAMA_QUERY);
            ps.setString(1, programaId);
            ResultSet rs = ps.executeQuery();

            while (rs.next())
                auspiciantes.add(
                        new Productor(
                                rs.getInt("id"),
                                rs.getString("nombre"),
                                rs.getString("apellido"),
                                rs.getString("programa")
                        )
                );

            return auspiciantes;
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                //no hago nada
            }
            throw new RadioException("Ocurri\u00F3 un error al obtener los productores del programa " + programaId, e);
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                //no hago nada
            }
        }
    }
}
