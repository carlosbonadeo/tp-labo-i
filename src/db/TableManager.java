package db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import exceptions.RadioException;

public class TableManager {

    public void createUserTable() throws RadioException {

        Connection c = DBManager.getInstance().connect();

        String sql = "CREATE TABLE usuarios (id INTEGER IDENTITY, username VARCHAR(256), nombre VARCHAR(256), mail VARCHAR(256), rol INTEGER, password VARCHAR(256))";

        try {
            Statement s = c.createStatement();
            s.execute(sql);
        } catch (SQLException e) {
            try {
                c.rollback();
                //e.printStackTrace();
                throw new RadioException("La tabla Usuarios ya existe, no se crea.");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    }

    public void dropUserTable() {

        Connection c = DBManager.getInstance().connect();

        String sql = "DROP TABLE usuarios";

        try {
            Statement s = c.createStatement();
            s.execute(sql);
            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void createProgramaTable() throws RadioException {

        Connection c = DBManager.getInstance().connect();

        String sql = "CREATE TABLE programas (id INTEGER IDENTITY, nombre VARCHAR(256), horario VARCHAR(256), conductores VARCHAR(256), gastos DOUBLE)";

        try {
            Statement s = c.createStatement();
            s.execute(sql);
        } catch (SQLException e) {
            try {
                c.rollback();
                //e.printStackTrace();
                throw new RadioException("La tabla Programas ya existe, no se crea.");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    }

    public void dropProgramaTable() {

    	Connection c = DBManager.getInstance().connect();

        String sql = "DROP TABLE programas";

        try {
            Statement s = c.createStatement();
            s.execute(sql);
            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void createAuspicianteTable() throws RadioException {

    	Connection c = DBManager.getInstance().connect();

        String sql = "CREATE TABLE auspiciantes (id INTEGER IDENTITY, nombre VARCHAR(256), cuota DOUBLE, activo BOOLEAN, programa VARCHAR(256))";

        try {
            Statement s = c.createStatement();
            s.execute(sql);
        } catch (SQLException e) {
            try {
                c.rollback();
                //e.printStackTrace();
                throw new RadioException("La tabla Auspiciantes ya existe, no se crea.");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    }

    public void dropAuspicianteTable() {

    	Connection c = DBManager.getInstance().connect();

        String sql = "DROP TABLE auspiciantes";

        try {
            Statement s = c.createStatement();
            s.execute(sql);
            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void createProductorTable() throws RadioException {

        Connection c = DBManager.getInstance().connect();

        String sql = "CREATE TABLE productores (id INTEGER IDENTITY, nombre VARCHAR(256), apellido VARCHAR(256), programa VARCHAR(256))";

        try {
            Statement s = c.createStatement();
            s.execute(sql);
        } catch (SQLException e) {
            try {
                c.rollback();
                //e.printStackTrace();
                throw new RadioException("La tabla Productores ya existe, no se crea.");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    }

    public void dropProductorTable() {

        Connection c = DBManager.getInstance().connect();

        String sql = "DROP TABLE productores";

        try {
            Statement s = c.createStatement();
            s.execute(sql);
            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
