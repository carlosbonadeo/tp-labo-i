package bussines;

import entitys.Auspiciante;
import entitys.Programa;
import exceptions.RadioException;
import interfaces.AuspicianteDAO;

import java.util.List;

public class AuspicianteBO {
    private AuspicianteDAO auspicianteDAO;
    private static final String ERROR_NAME_MSG = "El nombre del auspiciante debe ser mayor a cuatro caracteres";
    
    public void newAuspiciante(Auspiciante auspiciante) throws RadioException {
        validacionNombre(auspiciante.getNombre());
        auspicianteDAO.newAuspiciante(auspiciante);
    }

    public List<Auspiciante> getAuspiciantes() throws RadioException {
        return auspicianteDAO.getAuspiciantes();
    }

    public void deleteAuspiciante(Auspiciante auspiciante) throws RadioException {
        auspicianteDAO.deleteAuspiciante(auspiciante);
    }

    public void editAuspiciante(Auspiciante auspiciante) throws RadioException {
        validacionNombre(auspiciante.getNombre());
        auspicianteDAO.editAuspiciante(auspiciante);
    }

    public void setAuspicianteDAO(AuspicianteDAO auspicianteDAO) {
		this.auspicianteDAO = auspicianteDAO;
	}

    public List<Auspiciante> getAuspiciantesByPrograma(Programa programa) throws RadioException{
        return auspicianteDAO.getAuspiciantesByPrograma(programa);
    }

    private void validacionNombre(String nombre) throws RadioException {
        if (nombre.length() < 4)
            throw new RadioException(ERROR_NAME_MSG);
    }
}
