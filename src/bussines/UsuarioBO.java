package bussines;

import entitys.Usuario;
import exceptions.RadioException;
import interfaces.UsuarioDAO;
import ui.dialogs.DialogConfirm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UsuarioBO {
    private Usuario currentUser;
    private UsuarioDAO usuarioDAO;

    private void setAdminUser() {
        Usuario usuario = new Usuario();
        usuario.setUsername("admin");
        usuario.setName("Administrador");
        usuario.setRol(0);

        setCurrentUser(usuario);
    }

    public Usuario getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(Usuario currentUser) {
        this.currentUser = currentUser;
    }

    public void newUsuario(Usuario usuario) throws RadioException {
        Usuario user = getUsuarioByUsername(usuario.getUsername());

        if (user != null && user.getUsername().equals("ADMIN")) {
            usuarioDAO.editUsuario(usuario);
            return;
        }
        if (user != null && user.getUsername().equals(usuario.getUsername()))
            throw new RadioException("\u00A1El usuario ya existe!");

        usuarioDAO.newUsuario(usuario);
    }

    public List<Usuario> getUsuarios() throws RadioException {
        return usuarioDAO.getUsuarios();
    }

    public void deleteUsuario(Usuario usuario) throws RadioException {
        if (currentUser.getUsername().equals(usuario.getUsername()))
            throw new RadioException("\u00A1El usuario logueado no se puede eliminar a s\u00ED mismo!");
        usuarioDAO.deleteUsuario(usuario);
    }

    public void deleteUsuarios() throws RadioException {
        usuarioDAO.deleteUsuarios();
    }

    public void editUsuario(Usuario usuario) throws RadioException {
        usuarioDAO.editUsuario(usuario);
    }

    public Usuario getUsuarioByUsername(String username) throws RadioException {
        return usuarioDAO.getUsuarioByUsername(username);
    }

    public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
        this.usuarioDAO = usuarioDAO;
    }

    public void login(String user, char[] password) throws RadioException {
        Usuario usuario = usuarioDAO.getUsuarioByUsername(user);

        if (usuario != null && usuario.getPassword().equals(new String(password)))
            setCurrentUser(usuario);
        else
            throw new RadioException("\u00A1Usuario y/o contrase\u00F1a inv\u00E1lido!");
    }

    public List<String> getRoles() {
        List<String> roles = new ArrayList<String>();
        roles.add("Administrador");
        roles.add("Usuario");

        return roles;
    }

    public String getRol(int rol) {
        return getRoles().get(rol);
    }

    public void setAdminPassword(char[] password) throws RadioException {
        Usuario usuario = usuarioDAO.getUsuarioByUsername("ADMIN");

        if (usuario == null) {
            Usuario admin = new Usuario();
            admin.setUsername("ADMIN");
            admin.setName("Administrador");
            admin.setRol(0);
            admin.setPassword(new String(password));

            newUsuario(admin);
        } else {
            usuario.setPassword(new String(password));
            editUsuario(usuario);
        }

    }
}
