package bussines;

import entitys.Productor;
import entitys.Programa;
import exceptions.RadioException;
import interfaces.ProductorDAO;

import java.util.List;

public class ProductorBO {
    private ProductorDAO productorDAO;
    private static final String ERROR_NAME_MSG = "El nombre del auspiciante debe ser mayor a cuatro caracteres";

    public void newProductor(Productor productor) throws RadioException {
        validacionNombre(productor.getNombre());
        productorDAO.newProductor(productor);
    }

    public List<Productor> getProductores() throws RadioException {
        return productorDAO.getProductores();
    }

    public void deleteProductor(Productor productor) throws RadioException {
        productorDAO.deleteProductor(productor);
    }

    public void editProductor(Productor productor) throws RadioException {
        validacionNombre(productor.getNombre());
        productorDAO.editProductor(productor);
    }

    public void setProductorDAO(ProductorDAO productorDAO) {
        this.productorDAO = productorDAO;
    }

    public List<Productor> getAuspiciantesByPrograma(Programa programa) throws RadioException{
        return productorDAO.getProductoresByPrograma(programa);
    }

    private void validacionNombre(String nombre) throws RadioException {
        if (nombre.length() < 4)
            throw new RadioException(ERROR_NAME_MSG);
    }
}
