package bussines;

import java.util.List;

import entitys.Auspiciante;
import entitys.Programa;
import exceptions.RadioException;
import interfaces.ProgramaDAO;

public class ProgramaBO {
    private ProgramaDAO programaDAO;
    private AuspicianteBO auspicianteBO;
    private static final String ERROR_NAME_MSG = "El nombre del programa debe ser mayor a cuatro caracteres";

    public void newPrograma(Programa programa) throws RadioException {
        validacionNombre(programa.getNombre());
        programaDAO.newPrograma(programa);
    }

    public List<Programa> getProgramas() throws RadioException {
        return programaDAO.getProgramas();
    }

    public void deletePrograma(Programa programa) throws RadioException {
        List<Auspiciante> auspiciantes = auspicianteBO.getAuspiciantesByPrograma(programa);

        try {
            // Busco los auspiciantes asociados y borro la referencia al programa

            for (int i = 0; i < auspiciantes.size(); i++) {
                Auspiciante auspiciante = auspiciantes.get(i);
                auspiciante.setPrograma("");
                auspicianteBO.editAuspiciante(auspiciante);
            }

            programaDAO.deletePrograma(programa);
        } catch (RadioException e) {
        	// Rollback manual
            String programaId = programa.getId() + "-" + programa.getNombre();

            for (int i = 0; i < auspiciantes.size(); i++) {
                Auspiciante auspiciante = auspiciantes.get(i);
                auspiciante.setPrograma(programaId);
                auspicianteBO.editAuspiciante(auspiciante);
            }
        }

    }

    public void editPrograma(Programa programa) throws RadioException {
        validacionNombre(programa.getNombre());
        programaDAO.editPrograma(programa);

        // Busco los auspiciantes asociados y actualizo el nombre
        List<Auspiciante> auspiciantes = auspicianteBO.getAuspiciantesByPrograma(programa);

        for (int i = 0; i < auspiciantes.size(); i++) {
            Auspiciante auspiciante = auspiciantes.get(i);
            auspiciante.setPrograma(String.format("%d-%s", programa.getId(), programa.getNombre()));
            auspicianteBO.editAuspiciante(auspiciante);
        }
    }
    
    public void setProgramaDAO(ProgramaDAO programaDAO) {
		this.programaDAO = programaDAO;
	}

	public void setAuspicianteBO(AuspicianteBO auspicianteBO) {
        this.auspicianteBO = auspicianteBO;
    }

    private void validacionNombre(String nombre) throws RadioException {
        if (nombre.length() < 4)
            throw new RadioException(ERROR_NAME_MSG);
    }
}
