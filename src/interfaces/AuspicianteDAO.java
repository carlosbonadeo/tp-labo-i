package interfaces;

import entitys.Auspiciante;
import entitys.Programa;
import exceptions.RadioException;

import java.util.List;

public interface AuspicianteDAO {
    public void newAuspiciante(Auspiciante auspiciante) throws RadioException;
    public void deleteAuspiciante(Auspiciante auspiciante) throws RadioException;
    public void editAuspiciante(Auspiciante auspiciante) throws RadioException;
    public List<Auspiciante> getAuspiciantes() throws RadioException;
    public List<Auspiciante> getAuspiciantesByPrograma(Programa programa) throws RadioException;
}
