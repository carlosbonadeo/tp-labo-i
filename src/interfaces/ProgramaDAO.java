package interfaces;

import entitys.Programa;
import exceptions.RadioException;

import java.util.List;

public interface ProgramaDAO {
    public void newPrograma(Programa programa) throws RadioException;
    public void deletePrograma(Programa programa) throws RadioException;
    public void editPrograma(Programa programa) throws RadioException;
    public List<Programa> getProgramas() throws RadioException;
}
