package interfaces;

import entitys.Productor;
import entitys.Programa;
import exceptions.RadioException;

import java.util.List;

public interface ProductorDAO {
    public void newProductor(Productor productor) throws RadioException;
    public void deleteProductor(Productor productor) throws RadioException;
    public void editProductor(Productor productor) throws RadioException;
    public List<Productor> getProductores() throws RadioException;
    public List<Productor> getProductoresByPrograma(Programa programa) throws RadioException;
}
