package interfaces;

import entitys.Usuario;
import exceptions.RadioException;

import java.util.List;

public interface UsuarioDAO {
    public void newUsuario(Usuario usuario) throws RadioException;
    public void deleteUsuario(Usuario usuario) throws RadioException;
    public void editUsuario(Usuario usuario) throws RadioException;
    public List<Usuario> getUsuarios() throws RadioException;
    public Usuario getUsuarioByUsername(String username) throws RadioException;
    public void deleteUsuarios() throws RadioException;
}
