package handlers;

import bussines.AuspicianteBO;
import bussines.ProductorBO;
import bussines.ProgramaBO;
import bussines.UsuarioBO;
import db.AuspicianteImplDAO;
import db.ProductorImplDAO;
import db.ProgramaImplDAO;
import db.UsuarioImplDAO;
import entitys.Auspiciante;
import entitys.Productor;
import entitys.Programa;
import entitys.Usuario;
import exceptions.RadioException;
import ui.*;
import ui.auspiciante.AuspicianteABMPanel;
import ui.auspiciante.EditAuspiciantePanel;
import ui.auspiciante.NewAuspiciantePanel;
import ui.dialogs.DialogConfirm;
import ui.dialogs.RadioDialog;
import ui.home.HomePanel;
import ui.login.LoginPanel;
import ui.productor.EditProductorPanel;
import ui.productor.NewProductorPanel;
import ui.productor.ProductorABMPanel;
import ui.productor.ProductorTableModel;
import ui.programa.CalcularBalancePanel;
import ui.programa.NewProgramaPanel;
import ui.programa.ProgramaABMPanel;
import ui.programa.EditProgramaPanel;
import ui.usuario.*;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class RadioHandler {
    private UsuarioBO usuarioBO;
    private ProgramaBO programaBO;
    private AuspicianteBO auspicianteBO;
    private ProductorBO productorBO;
    private RadioMainFrame radioMainFrame;

    public RadioHandler() {
        usuarioBO = new UsuarioBO();
    	programaBO = new ProgramaBO();
    	auspicianteBO = new AuspicianteBO();
    	productorBO = new ProductorBO();
    	radioMainFrame = new RadioMainFrame("Radio", this);
    	usuarioBO.setUsuarioDAO(new UsuarioImplDAO());
    	programaBO.setProgramaDAO(new ProgramaImplDAO());
    	programaBO.setAuspicianteBO(new AuspicianteBO());
    	auspicianteBO.setAuspicianteDAO(new AuspicianteImplDAO());
    	productorBO.setProductorDAO(new ProductorImplDAO());
    }

    public void initMainFrame() {
        showLoginPanel();
        radioMainFrame.setVisible(true);
    }

    public void showLoginPanel() {
        radioMainFrame.changePanel(new LoginPanel(this));
    }

    public void showHomePanel() {
        radioMainFrame.changePanel(new HomePanel(this));
    }

//  USUARIO ----------------------------------------------------------------------------------

    public void login(String user, char[] password) {
        try {
            usuarioBO.login(user, password);
            loginSuccess();
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void loginSuccess() {
        radioMainFrame.setVisibleRadioMenuBar(true);
        showHomePanel();
    }

    public void logout() {
        if (DialogConfirm.show("Cerrar sesi\u00F3n", "\u00BFEst\u00E1s seguro que quer\u00E9s cerrar sesi\u00F3n?")) {
            radioMainFrame.setVisibleRadioMenuBar(false);
            showLoginPanel();
        }
    }

    public void setCurrentUser(Usuario usuario) {
        usuarioBO.setCurrentUser(usuario);
    }

    public Usuario getCurrentUser() {
        return usuarioBO.getCurrentUser();
    }

    public List<Usuario> getUsuarios() {
        List<Usuario> usuarios = new ArrayList<Usuario>();

        try {
            usuarios = usuarioBO.getUsuarios();
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }

        return usuarios;
    }

    public boolean isAdmin() {
        if (getCurrentUser() != null && getCurrentUser().getRol() != 0) {
            RadioDialog.error("No ten\u00E9s permiso para ver esta secci\u00F3n");
            return false;
        }
        return true;
    }

    public void showNewUsuarioPanel() {
        if (isAdmin())
            radioMainFrame.changePanel(new NewUsuarioPanel(this));
    }

    public void showUsuarioTablePanel() {
        if (isAdmin())
            radioMainFrame.changePanel(new UsuarioABMPanel(this));
    }

    public void hardReset() {
        try {
            if (isAdmin() && DialogConfirm.show("Hard reset", "Est\u00E1s a punto de borrar todos los usuarios, esta acci\u00F3n no se puede deshacer, \u00BFest\u00E1s seguro de continuar?")) {
                usuarioBO.deleteUsuarios();
                radioMainFrame.setVisibleRadioMenuBar(false);
                radioMainFrame.changePanel(new JPanel());
                RadioDialog.success("Se han borrado todos los usuarios, saliendo del sistema.");
                showLoginPanel();
            }
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void setAdminPassword(char[] password) {
        try {
            usuarioBO.setAdminPassword(password);
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void newUsuario(Usuario usuario) {
        try {
            usuarioBO.newUsuario(usuario);
            RadioDialog.success("\u00A1Usuario agregado correctamente!");
            showUsuarioTablePanel();
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void deleteUsuario(Usuario usuario) {
        try {
            usuarioBO.deleteUsuario(usuario);
            RadioDialog.success("\u00A1Usuario borrado correctamente!");
            showUsuarioTablePanel();
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void editUsuario(Usuario usuario) {
        try {
            usuarioBO.editUsuario(usuario);
            RadioDialog.success("\u00A1Ususario guardado correctamente!");
            showUsuarioTablePanel();
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void showUpdateUsuario(Usuario usuario) {
        radioMainFrame.changePanel(new EditUsuarioPanel(usuario, this));
    }

    public Usuario getUsuarioByUsername(String username) {
        Usuario usuario;

        try {
            usuario = usuarioBO.getUsuarioByUsername(username);
            return usuario;
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    public List<String> getRoles() {
        return usuarioBO.getRoles();
    }

    public String getRol(int rol) {
        return usuarioBO.getRol(rol);
    }

    public void showFirstLoginDialog() {
        new CreateAdminUser(this);
    }

    public void showUserInfoPopUp() {
        new UserInfoPanel(this, getCurrentUser());
    }

//  PROGRAMA ----------------------------------------------------------------------------------

    public List<Programa> getProgramas() {
        List<Programa> programas = new ArrayList<Programa>();

        try {
            programas = programaBO.getProgramas();
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }

        return programas;
    }

    public void showNewProgramaPanel() {
        radioMainFrame.changePanel(new NewProgramaPanel(this));
    }

    public void showProgramasTablePanel() {
    	radioMainFrame.changePanel(new ProgramaABMPanel(this));
    }

    public void newPrograma(Programa programa) {
        try {
            programaBO.newPrograma(programa);
            RadioDialog.success("\u00A1Programa agregado correctamente!");
            showProgramasTablePanel();
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void deletePrograma(Programa programa) {
        try {
            programaBO.deletePrograma(programa);
            RadioDialog.success("\u00A1Programa borrado correctamente!");
            showProgramasTablePanel();
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void editPrograma(Programa programa) {
        try {
            programaBO.editPrograma(programa);
            RadioDialog.success("\u00A1Programa guardado correctamente!");
            showProgramasTablePanel();
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void showUpdatePrograma(Programa programa) {
    	radioMainFrame.changePanel(new EditProgramaPanel(programa, this));
    }

    //  AUSPICIANTE ----------------------------------------------------------------------------------

    public List<Auspiciante> getAuspiciantes() {
        List<Auspiciante> auspiciantes = new ArrayList<Auspiciante>();

        try {
            auspiciantes = auspicianteBO.getAuspiciantes();
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }

        return auspiciantes;
    }

    public void showNewAuspiciantePanel() {
        radioMainFrame.changePanel(new NewAuspiciantePanel(this));
    }

    public void showAuspiciantesTablePanel() {
        radioMainFrame.changePanel(new AuspicianteABMPanel(this));
    }

    public void newAuspiciante(Auspiciante auspiciante) {
        try {
            auspicianteBO.newAuspiciante(auspiciante);
            RadioDialog.success("\u00A1Auspiciante agregado correctamente!");
            showAuspiciantesTablePanel();
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void deleteAuspiciante(Auspiciante auspiciante) {
        try {
            auspicianteBO.deleteAuspiciante(auspiciante);
            RadioDialog.success("\u00A1Auspiciante borrado correctamente!");
            showAuspiciantesTablePanel();
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void editAuspiciante(Auspiciante auspiciante) {
        try {
            auspicianteBO.editAuspiciante(auspiciante);
            RadioDialog.success("\u00A1Auspiciante guardado correctamente!");
            showAuspiciantesTablePanel();
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void showUpdateAuspiciante(Auspiciante auspiciante) {
        radioMainFrame.changePanel(new EditAuspiciantePanel(auspiciante, this));
    }

    public void showCalcularBalancePanel(Programa programa) {
        radioMainFrame.changePanel(new CalcularBalancePanel(programa, this));
    }

    public List<Auspiciante> getAuspiciantesByPrograma(Programa programa) {
        List<Auspiciante> auspiciantes = new ArrayList<Auspiciante>();

        try {
            auspiciantes = auspicianteBO.getAuspiciantesByPrograma(programa);
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }

        return auspiciantes;
    }

    //  PRODUCTOR ----------------------------------------------------------------------------------

    public void editProductor(Productor productor) {
        try {
            productorBO.editProductor(productor);
            RadioDialog.success("\u00A1Productor guardado correctamente!");
            showProductoresTablePanel();
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void showProductoresTablePanel() {
        radioMainFrame.changePanel(new ProductorABMPanel(this));
    }

    public void newProductor(Productor productor) {
        try {
            productorBO.newProductor(productor);
            RadioDialog.success("\u00A1Productor agregado correctamente!");
            showProductoresTablePanel();
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void showNewProductorPanel() {
        radioMainFrame.changePanel(new NewProductorPanel(this));
    }

    public List<Productor> getProductores() {
        List<Productor> productores = new ArrayList<Productor>();

        try {
            productores = productorBO.getProductores();
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }

        return productores;
    }

    public void showUpdateProductor(Productor productor) {
        radioMainFrame.changePanel(new EditProductorPanel(productor, this));
    }

    public void deleteProductor(Productor productor) {
        try {
            productorBO.deleteProductor(productor);
            RadioDialog.success("\u00A1Productor borrado correctamente!");
            showAuspiciantesTablePanel();
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public List<Productor> getProductoresByPrograma(Programa programa) {
        List<Productor> productores = new ArrayList<Productor>();

        try {
            productores = productorBO.getAuspiciantesByPrograma(programa);
        } catch (RadioException e) {
            RadioDialog.error(e.getMessage());
            e.printStackTrace();
        }

        return productores;
    }

    public void showProductoresDialog(Programa programa) {
        List<Productor> productores = getProductoresByPrograma(programa);

        JTable table = new JTable(new ProductorTableModel(productores));
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scroll = new JScrollPane(table);
        scroll.setPreferredSize(new Dimension(300, 200));

        RadioDialog.popup("Productores de " + programa.getNombre(), scroll);
    }
}
